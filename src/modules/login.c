/*
    Copyright (C) 2001-2015 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <time.h>
#include <pwd.h>
#include <ctype.h>

#ifdef HAVE_LIMITS_H
#include <limits.h>
#ifndef LINE_MAX
#ifdef _POSIX2_LINE_MAX
#define LINE_MAX _POSIX2_LINE_MAX
#else
#define LINE_MAX 2048
#endif
#endif
#endif

#ifndef HAVE_STRSEP
#include "../strsep.c"
#endif

#ifndef HAVE_ERR_H
#include "../err.c"
#endif

#include "login.h"

#define LOGIN_OPTION_ORDER	"pdimyhtl"
#define LOGIN_OPTION_STRING	"Lpdimyhtl:"

static char options[9];		/* NULL terminated. */
static char *last_options;
static char **strings;
static time_t now;
static int login_count;
#if (!defined(__FreeBSD_version) || __FreeBSD_version < 900000)
static int lastlogfd;
#endif

void add_string(char ***, const char *);
char *stamp(time_t, const char *);
char *safe_strncat(char *, const char *, size_t);

void ui_module_init(int *chainable)
{
    *chainable = 0;
    time(&now);
}

void ui_module_exit()
{
#ifdef HAVE_PROCFS
    if (procdir)
	closedir(procdir);
#endif

#ifdef HAVE_KVM_H
    if (kd)
	kvm_close(kd);
#endif

#if (!defined(__FreeBSD_version) || __FreeBSD_version < 900000)
    if (lastlogfd)
	close(lastlogfd);
#endif
}

static void free_logins(UTMP **u)
{
    if (!u)
        return;

    if (login_count) {
	UTMP **up;

	for (up = u; *up; up++)
	    free(*up);

	free(u);
    }
}

#ifndef HAVE_UTMPX_H
/* This is for *BSD (login process id). */
#ifdef BSD_KVM
static char *get_pid(uid_t uid, int multi)
{
    static int firstrun;
    static char line[LINE_MAX];
    int cnt, i;
    char errbuf[LINE_MAX];
    struct kinfo_proc *kp;

    line[0] = '\0';

    if (!kd && firstrun)
	return "!";

    if (!kd) {
	firstrun = 1;

#if defined(__NetBSD__) || defined (__OpenBSD__)
	if ((kd = kvm_openfiles(NULL, NULL, NULL, O_RDONLY, errbuf)) == NULL) {
#else
	if ((kd = kvm_openfiles(_PATH_DEVNULL, _PATH_DEVNULL, _PATH_DEVNULL,
				O_RDONLY, errbuf)) == NULL) {
#endif
	    warnx("%s", errbuf);
	    return "!";
	}
    }

#ifdef __OpenBSD__
    if ((kp = kvm_getprocs(kd, KERN_PROC_UID, uid, sizeof(struct kinfo_proc),
	 &cnt)) == NULL) {
	warnx("kvm_getprocs(): %s", kvm_geterr(kd));
	return "!";
    }
#else
    if ((kp = kvm_getprocs(kd, KERN_PROC_UID, uid, &cnt)) == NULL) {
	warnx("kvm_getprocs(): %s", kvm_geterr(kd));
	return "!";
    }
#endif

    for (i = 0; i < cnt; i++) {
	char buf[32];
	pid_t pid = 0;

#ifdef __OpenBSD__
	if ((kp[i].p_eflag & EPROC_SLEADER) && kp[i].p_tdev != -1)
	    pid = kp[i].p_pid;
#else
#if __FreeBSD_version < 500000
	if (kp[i].kp_eproc.e_flag & EPROC_SLEADER && kp[i].kp_eproc.e_tdev
	    != -1) {
	    pid = kp[i].kp_eproc.e_ppid;
	    /*
	     * pid = kp[i].kp_proc.p_pid;
	     */
	}
#else
	if (kp[i].ki_kiflag & KI_SLEADER && kp[i].ki_tdev != -1) {
	    pid = kp[i].ki_pid;
	}
#endif
#endif
	if (!pid || pid == 1)
	    continue;

        snprintf(buf, sizeof(buf), "%i%c", pid, multi);
	safe_strncat(line, buf, sizeof(line));
    }

    if (line[0] == '\0')
	return "!";

    line[strlen(line) - 1] = '\0';
    return line;
}

/* This is for Linux and Solaris. */
#elif defined(HAVE_PROCFS)
#include <sys/types.h>
#include <sys/stat.h>

#ifdef HAVE_DIRENT_H
#include <dirent.h>
#endif

#ifdef __sun__
#include <unistd.h>
#include <procfs.h>
#endif

static char *get_pid(uid_t uid, int multi)
{
    static int firstrun;
    struct dirent *ent;
    struct stat st;
    static char line[LINE_MAX];
    pid_t *pids = 0, *tpids;
    int pid_index = 0;

#ifdef __sun__
    int fd;
    struct pstatus pstat;
#else
    FILE *fp;
#endif

    line[0] = '\0';

    if (!procdir && firstrun)
	return "!";

    if (!procdir) {
	firstrun = 1;

	if ((procdir = opendir("/proc")) == NULL) {
	    warn("%s", "/proc");
	    return "!";
	}
    }

    rewinddir(procdir);

again:
    while ((ent = readdir(procdir)) != NULL) {
	pid_t pid = -1;
	char filename[FILENAME_MAX];
	char buf[LINE_MAX];
	int i;

#ifndef __sun__
	char *t;
#endif

	if (!isdigit((unsigned char) *ent->d_name))
	    continue;

#ifdef __linux__
	snprintf(filename, sizeof(filename), "/proc/%s/stat", ent->d_name);
#else
	snprintf(filename, sizeof(filename), "/proc/%s/status", ent->d_name);
#endif

	if (stat(filename, &st) == -1)
	    continue;

	/*
	 * The current user owns this file (process id).
	 */
	if (st.st_uid == uid) {
#ifdef __sun__
	    if ((fd = open(filename, O_RDONLY)) == -1)
		continue;

	    if (pread(fd, &pstat, sizeof(struct pstatus), 0) !=
		sizeof(struct pstatus)) {
		close(fd);
		continue;
	    }

	    pid = pstat.pr_ppid;
	    close(fd);
#else
	    if ((fp = fopen(filename, "r")) == NULL)
		continue;

	    if ((t = fgets(buf, sizeof(buf), fp)) == NULL) {
		fclose(fp);
		continue;
	    }

#ifdef __linux__
	    if ((i = sscanf(buf, "%*i %*s %*c %*i %*i %i", &pid)) < 1) {
#endif
/*
#else
	    if ((i = sscanf(buf, "%*s %*i %li", &ppid)) < 1) {
#endif
*/
		fclose(fp);
		continue;
	    }

	    fclose(fp);
#endif

	    /*
	     * Skip duplicate pids.
	     */
	    for (i = 0; i < pid_index; i++) {
		if (pids[i] == pid)
		    goto again;
	    }

	    snprintf(buf, sizeof(buf), "%li%c", (unsigned long) pid, multi);
	    safe_strncat(line, buf, sizeof(line));

	    if ((tpids =
		 realloc(pids, (pid_index + 2) * sizeof(pid_t *))) == NULL) {
		warn("realloc()");
		continue;
	    }

	    pids = tpids;
	    pids[pid_index++] = pid;
	}
    }

    if (pid_index)
	free(pids);

    if (line[0] == '\0')
	return "!";

    line[strlen(line) - 1] = '\0';
    return line;
}
#else
/* Unsupported OS. */
static char *get_pid(uid_t uid, int multi)
{
    return "!";
}
#endif
#endif

/* Break up the last login string into sections and add the sections to the
 * output string array if needed. */
static void last_strings(char *str)
{
    int i = 0;
    char *buf;
    const char *line, *host, *when;

    line = host = when = (str) ? "-" : "!";

    while ((buf = strsep(&str, ",")) != NULL) {
	if (!buf[0])
	    continue;

	switch (i++) {
	    case 0:
		line = buf;
		break;
	    case 1:
		host = buf;
		break;
	    case 2:
		when = buf;
		break;
	    default:
		break;
	}
    }

    for (i = 0; i < strlen(last_options); i++) {
	switch (last_options[i]) {
	    case 'y':
		add_string(&strings, line);
		break;
	    case 'h':
		add_string(&strings, host);
		break;
	    case 't':
		add_string(&strings, when);
		break;
	    case 'a':
		add_string(&strings, line);
		add_string(&strings, host);
		add_string(&strings, when);
	    default:
		break;
	}
    }
}

/* Get the lastlog structure from the lastlog file. */
#ifdef HAVE_GETLASTLOGX
static char *lastlogin(const struct passwd *pw, char *tf)
{
    struct lastlogx *last =  getlastlogx(_PATH_LASTLOGX, pw->pw_uid, NULL);
    static char buf[LINE_MAX];

    if (!last)
	return NULL;

    snprintf(buf, sizeof(buf), "%s,%s,%s",
	!last->ll_line[0] ? "!" : last->ll_line,
	(!last->ll_host[0] || !isalnum(last->ll_host[0])) ?  "-" : last->ll_host,
	stamp(last->ll_tv.tv_sec, tf));
    return buf;
}
#else
#if __FreeBSD_version >= 900000
static char *lastlogin(const struct passwd *pw, char *tf)
{
    struct utmpx *last;
    static char buf[LINE_MAX];

    if (setutxdb(UTXDB_LASTLOGIN, NULL) == -1) {
	warn("lastlog");
	return NULL;
    }

    last = getutxuser(pw->pw_name);

    if (!last)
	return NULL;

    snprintf(buf, sizeof(buf), "%s,%s,%s",
	!last->ut_line[0] ? "!" : last->ut_line,
	(!last->ut_host[0] || !isalnum(last->ut_host[0])) ?  "-" : last->ut_host,
	stamp(last->ut_tv.tv_sec, tf));

    return buf;
}
#else
static char *lastlogin(const struct passwd *pw, char *tf)
{
    int count;
    long offset;
    static char buf[LINE_MAX];
    struct lastlog last;

    if (lastlogfd < 0)
	return NULL;

    if (!lastlogfd) {
	if ((lastlogfd = open(_PATH_LASTLOG, O_RDONLY)) == -1) {
	    warn("%s", _PATH_LASTLOG);
	    return NULL;
	}
    }

    offset = (long) pw->pw_uid * sizeof(struct lastlog);

    if (lseek(lastlogfd, offset, SEEK_SET) == -1) {
	warn("%s", _PATH_LASTLOG);
	return NULL;
    }

    if ((count = read(lastlogfd, &last, sizeof(struct lastlog))) !=
	sizeof(struct lastlog)) {
	if (count == -1)
	    warn("%s", _PATH_LASTLOG);

	return NULL;
    }

#ifdef __NetBSD__
#ifdef HAVE_UTMPX_H
    last.ll_host[UTX_HOSTSIZE-1] = '\0';
    last.ll_line[UTX_LINESIZE-1] = '\0';
#else
    last.ll_host[UT_HOSTSIZE-1] = '\0';
    last.ll_line[UT_LINESIZE-1] = '\0';
#endif
#else
    last.ll_host[UT_HOSTSIZE-1] = '\0';
    last.ll_line[UT_LINESIZE-1] = '\0';
#endif

    snprintf(buf, sizeof(buf), "%s,%s,%s",
	!last.ll_line[0] ? "!" : last.ll_line,
	(!last.ll_host[0] || !isalnum(last.ll_host[0])) ?
	    !isdigit(last.ll_line[3]) ? "!" : "-" : last.ll_host,
	!last.ll_time ? "!" : stamp(last.ll_time, tf));
    return buf;
}
#endif
#endif

/* This will return an array of utmp structures if a user is logged in, NULL
 * otherwise. We'll try to keep the utmp file descriptor open if possible to
 * speed things up a bit. */
static UTMP **get_utmp(const char *user)
{
    UTMP **logins = NULL;
#if defined HAVE_UTMPX_H && defined (HAVE_SETUTXENT)
    UTMP *u;
#else
    UTMP u;
    int count;
    static int fd;

    if (fd < 0)
	return NULL;

    if (!fd) {
	if ((fd = open(_PATH_UTMP, O_RDONLY)) == -1) {
	    warn("%s", _PATH_UTMP);
	    return NULL;
	}
    }
#endif

    login_count = 0;

#if defined HAVE_UTMPX_H && defined (HAVE_SETUTXENT)
    setutxent();

    while ((u = getutxent()) != NULL) {
	if (!strncmp(u->ut_user, user, UT_NAMESIZE) && u->ut_type != DEAD_PROCESS) {
#else
    lseek(fd, 0, SEEK_SET);

    while ((count = read(fd, &u, sizeof(UTMP))) == sizeof(UTMP)) {
	if (strncmp(u.ut_name, user, UT_NAMESIZE) == 0) {
#endif
	    UTMP **tmp;

	    if ((tmp = realloc(logins,
				  (login_count + 2) * sizeof(UTMP *))) ==
		NULL) {
		warn("realloc()");
		free_logins(logins);
		return NULL;
	    }

	    logins = tmp;

	    if ((logins[login_count] = malloc(sizeof(UTMP))) == NULL) {
		warn("malloc()");
		free_logins(logins);
		return NULL;
	    }

#if defined HAVE_UTMPX_H && defined (HAVE_SETUTXENT)
#ifdef __NetBSD__
	    memcpy(logins[login_count]->ut_name, u->ut_name, UTX_NAMESIZE);
	    logins[login_count]->ut_name[UTX_NAMESIZE-1] = 0;
	    memcpy(logins[login_count]->ut_line, u->ut_line, UTX_LINESIZE);
	    logins[login_count]->ut_line[UTX_LINESIZE-1] = 0;
	    memcpy(logins[login_count]->ut_host, u->ut_host, UTX_HOSTSIZE);
	    logins[login_count]->ut_host[UTX_HOSTSIZE-1] = 0;
	    logins[login_count]->ut_pid = u->ut_pid;
#else
	    memcpy(logins[login_count]->ut_user, u->ut_user, UT_NAMESIZE);
	    logins[login_count]->ut_user[UT_NAMESIZE-1] = 0;
	    memcpy(logins[login_count]->ut_line, u->ut_line, UT_LINESIZE);
	    logins[login_count]->ut_line[UT_LINESIZE-1] = 0;
	    memcpy(logins[login_count]->ut_host, u->ut_host, UT_HOSTSIZE);
	    logins[login_count]->ut_host[UT_HOSTSIZE-1] = 0;
	    logins[login_count]->ut_tv.tv_sec = u->ut_tv.tv_sec;
	    logins[login_count]->ut_pid = u->ut_pid;
#endif
#else
	    memcpy(logins[login_count]->ut_name, u.ut_name, UT_NAMESIZE);
	    logins[login_count]->ut_name[UT_NAMESIZE-1] = 0;
	    memcpy(logins[login_count]->ut_line, u.ut_line, UT_LINESIZE);
	    logins[login_count]->ut_line[UT_LINESIZE-1] = 0;
	    memcpy(logins[login_count]->ut_host, u.ut_host, UT_HOSTSIZE);
	    logins[login_count]->ut_host[UT_HOSTSIZE-1] = 0;
	    logins[login_count]->ut_time = u.ut_time;
#endif
	    logins[++login_count] = NULL;
	}
    }

    return logins;
}

/* The 'mesg' status of the logged in user. */
static const char *msgstat(UTMP **u, int multi)
{
    static char line[LINE_MAX];
    int i;

    line[0] = '\0';

    for (i = 0; i < login_count; i++) {
	char filename[FILENAME_MAX];
	struct stat st;
	char m[2] = { multi, '\0' };

	snprintf(filename, sizeof(filename), "%s%s", _PATH_DEV, u[i]->ut_line);

	if (stat(filename, &st) == -1)
	    safe_strncat(line, "!", sizeof(line));
	else
	    safe_strncat(line,
		    (st.st_mode & S_IWGRP || st.st_mode & S_IWOTH) ? "1" : "0",
		    sizeof(line));

	safe_strncat(line, m, sizeof(line));
    }

    if (line[0] == '\0')
	return "!";

    line[strlen(line) - 1] = '\0';
    return line;
}

/* Returns the users idle time in seconds. */
static const char *idle(UTMP **u, int multi)
{
    static char line[LINE_MAX];
    time_t t;
    struct stat st;
    int i;

    line[0] = '\0';

    for (i = 0; i < login_count; i++) {
	char buf[FILENAME_MAX];
	char m[2] = { multi, '\0' };

	snprintf(buf, sizeof(buf), "%s%s", _PATH_DEV, u[i]->ut_line);

	if (stat(buf, &st) == -1) {
	    safe_strncat(line, "!", sizeof(line));
	    safe_strncat(line, m, sizeof(line));
	    continue;
	}

#ifdef HAVE_UTMPX_H
	if (u[i]->ut_tv.tv_sec > st.st_atime) {
#else
	if (u[i]->ut_time > st.st_atime) {
#endif
	    safe_strncat(line, "-", sizeof(line));
	    safe_strncat(line, m, sizeof(line));
	    continue;
	}

	t = st.st_atime;

#ifdef HAVE_UTMPX_H
	if (t < u[i]->ut_tv.tv_sec)
	    t = u[i]->ut_tv.tv_sec;
#else
	if (t < u[i]->ut_time)
	    t = u[i]->ut_time;
#endif

	snprintf(buf, sizeof(buf), "%lu", (now - t <= 0) ? 0 : now - t);
	safe_strncat(line, buf, sizeof(line));
	safe_strncat(line, m, sizeof(line));
    }

    if (line[0] == '\0')
	return "!";

    line[strlen(line) - 1] = '\0';
    return line;
}

/* This is output if the -h command line option is passed to the main program.
 */
void ui_module_help()
{
    printf("  Login information [-L (-%s)]:\n", LOGIN_OPTION_ORDER);
    printf("\t-y  tty\t\t\t\t");
    printf("-m  message status\n");
    printf("\t-t  login time stamp\t\t");
    printf("-d  duration in minutes\n");
    printf("\t-h  hostname\t\t\t");
    printf("-i  seconds idle\n");
    printf("\t-p  login process id\n");
    printf("\t-l  lastlog information"
	   " (any of tt[y],[h]ostname,[t]ime, or [a]ll)\n\n");
    return;
}

/* This is the equivalent to main() only without argc and argv available. */
int ui_module_exec(char ***s, const struct passwd *pw, const int multi,
	       const int verbose, char *tf)
{
    char *p = options;
    UTMP **u = NULL;
    char buf[255];

    login_count = 0;
    u = get_utmp(pw->pw_name);
    strings = *s;

    while (*p) {
	char line[LINE_MAX] = { '\0' };
	int i;
	char m[2] = { multi, '\0' };

	switch (*p) {
	    case 'i':
		add_string(&strings, (u) ? idle(u, multi) : "!");
		break;
	    case 'l':
		last_strings(lastlogin(pw, tf));
		break;
	    case 'h':
		for (i = 0; i < login_count; i++) {
		    if (u[i]->ut_host[0]
			    && isalnum((unsigned char) u[i]->ut_host[0]))
			safe_strncat(line, u[i]->ut_host, sizeof(line));
		    else
			safe_strncat(line, "-", sizeof(line));

		    safe_strncat(line, m, sizeof(line));
		}

		if (line[0] == '\0')
		    strncpy(line, "!", sizeof(line));
		else
		    line[strlen(line) - 1] = '\0';

		add_string(&strings, line);
		break;
	    case 'y':
		for (i = 0; i < login_count; i++) {
		    if (u[i]->ut_line[0])
			safe_strncat(line, u[i]->ut_line, sizeof(line));
		    else
			safe_strncat(line, "!", sizeof(line));

		    safe_strncat(line, m, sizeof(line));
		}

		if (line[0] == '\0')
		    strncpy(line, "!", sizeof(line));
		else
		    line[strlen(line) - 1] = '\0';

		add_string(&strings, line);
		break;
	    case 'm':
		add_string(&strings, u ? msgstat(u, multi) : "!");
		break;
	    case 't':
		for (i = 0; i < login_count; i++) {
#ifdef HAVE_UTMPX_H
		    safe_strncat(line, stamp(u[i]->ut_tv.tv_sec, tf), sizeof(line));
#else
		    safe_strncat(line, stamp(u[i]->ut_time, tf), sizeof(line));
#endif
		    safe_strncat(line, m, sizeof(line));
		}

		if (line[0] == '\0')
		    strncpy(line, "!", sizeof(line));
		else
		    line[strlen(line) - 1] = '\0';

		add_string(&strings, line);
		break;
	    case 'd':
		for (i = 0; i < login_count; i++) {
#ifdef HAVE_UTMPX_H
		    if ((now - u[i]->ut_tv.tv_sec) > 60) {
			snprintf(buf, sizeof(buf), "%lu",
				 ((now - u[i]->ut_tv.tv_sec) / 60));
#else
		    if ((now - u[i]->ut_time) > 60) {
			snprintf(buf, sizeof(buf), "%lu",
				 ((now - u[i]->ut_time) / 60));
#endif
			safe_strncat(line, buf, sizeof(line));
		    }
		    else
			safe_strncat(line, "-", sizeof(line));

		    safe_strncat(line, m, sizeof(line));
		}

		if (line[0] == '\0')
		    strncpy(line, "!", sizeof(line));
		else
		    line[strlen(line) - 1] = '\0';

		add_string(&strings, line);
		break;
	    case 'p':
#ifdef HAVE_UTMPX_H
		for (i = 0; i < login_count; i++) {
		    if (u[i]->ut_pid) {
			snprintf(buf, sizeof(buf), "%li", (long) u[i]->ut_pid);
			safe_strncat(line, buf, sizeof(line));
		    }
		    else
			safe_strncat(line, "!", sizeof(line));

		    safe_strncat(line, m, sizeof(line));
		}

		if (line[0] == '\0')
		    strncpy(line, "!", sizeof(line));
		else
		    line[strlen(line) - 1] = '\0';

		add_string(&strings, line);
#else
		add_string(&strings, (u) ? get_pid(pw->pw_uid, multi) : "!");
#endif
		break;
	    default:
		break;
	}

	p++;
    }

    free_logins(u);
    *s = strings;
    return EXIT_SUCCESS;
}

/* See if the last login options (-l) are valid. */
static int parse_last_options(const char *args)
{
    int i = 0;

    for (i = 0; i < strlen(args); i++) {
	switch (args[i]) {
	    case 'y':
	    case 'h':
	    case 't':
	    case 'a':
		break;
	    default:
		return 1;
	}
    }

    return 0;
}

const char *ui_module_options_init(char **defaults)
{
    *defaults = (char *)"L";
    return LOGIN_OPTION_STRING;
}

/* Check module option validity. */
int ui_module_options(int argc, char **argv)
{
    int opt;
    char *p = options;

    while ((opt = getopt(argc, argv, LOGIN_OPTION_STRING)) != -1) {
	switch (opt) {
	    case 'l':
		if (parse_last_options(optarg))
		    return 1;

		last_options = optarg;
		break;
	    case 'L':
		strncpy(options, LOGIN_OPTION_ORDER, sizeof(options));
		last_options = (char *)"a";
		return 0;
	    case 'p':
	    case 'd':
	    case 'i':
	    case 'm':
	    case 'y':
	    case 'h':
	    case 't':
		break;
	    case '?':
		warnx("login: invalid option -- %c", optopt);
	    default:
		return 1;
	}

	*p++ = opt;
	*p = '\0';
    }

    return 0;
}
