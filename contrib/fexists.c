/*
    Copyright (C) 2001-2011 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
/*
 * Userinfo module to check file existance in a users home directory. The only
 * required flag is -f to specify the filename which only checks for the file
 * existance and not mode or type. This module is not chainable. It gets the
 * users home directory from the password structure passed from the main
 * program.
 *
 * Compile with: cc -O2 -fPIC -shared -o fexists.so fexists.c
 * Run with: ui -O ./fexists.so -f filename --
 *
 * Ben Kibbey <bjk@luxsci.net>
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>
#include <pwd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

#define F_READ		0x0001
#define F_WRITE		0x0002
#define F_WRITE_W	0x0004
#define F_DIR		0x0008

static struct file_s {
    char *name;
    unsigned flags;
} *files;

static int idx;
extern void add_string(char ***, const char *);

void ui_module_init(int *chainable)
{
    *chainable = 0;
}

void ui_module_exit()
{
    int i;

    for (i = 0; i < idx; i++)
	free(files[i].name);

    free(files);
    files = NULL;
    idx = 0;
}

void ui_module_help()
{
    printf("  Check for file existance in a users home directory ([-rwWd] -f <file>).\n");
    printf("\t-r  filename is readable by you\n");
    printf("\t-w  filename is writeable\n");
    printf("\t-W  filename is writeable by you\n");
    printf("\t-d  filename is a directory\n\n");
}

char *ui_module_options_init(char **defaults)
{
    *defaults = NULL;
    return "rwWdf:";
}

int ui_module_options(int argc, char **argv)
{
    int opt;
    unsigned flags = 0;

    while ((opt = getopt(argc, argv, "f:rwWd")) != -1) {
	switch (opt) {
	    case 'r':
		flags |= F_READ;
		break;
	    case 'w':
		flags |= F_WRITE;
		break;
	    case 'W':
		flags |= F_WRITE_W;
		break;
	    case 'd':
		flags |= F_DIR;
		break;
	    case 'f':
		if (!flags)
		    return 1;

		if ((files = realloc(files, (idx + 1) * sizeof(struct file_s))) == NULL) {
		    warn("realloc()");
		    return 1;
		}

		files[idx].name = strdup(optarg);
		files[idx++].flags = flags;
		flags = 0;
		break;
	    default:
		return 1;
	}
    }

    return !files ? 1 : 0;
}

int ui_module_exec(char ***results, const struct passwd *pw, const int msep, const int verbose,
	const char *tf)
{
    char **strings = *results;
    char buf[FILENAME_MAX];
    struct stat st;
    int ret = 0;
    int i;

    for (i = 0; i < idx; i++) {
	ret = 0;
	snprintf(buf, sizeof(buf), "%s/%s", pw->pw_dir, files[i].name);

	if (stat(buf, &st) != -1) {
	    ret = 1;

	    if (files[i].flags & F_READ) {
		if (access(buf, R_OK) == -1) {
		    ret = 0;
		    goto done;
		}

		ret = 1;
	    }

	    if (files[i].flags & F_WRITE) {
		if (access(buf, W_OK) == -1) {
		    ret = 0;
		    goto done;
		}

		ret = 1;
	    }

	    if (files[i].flags & F_WRITE) {
		if (st.st_mode & S_IWOTH)
		    ret = 1;
		else {
		    ret = 0;
		    goto done;
		}
	    }

	    if (files[i].flags & F_DIR) {
		if (S_ISDIR(st.st_mode))
		    ret = 1;
		else {
		    ret = 0;
		    goto done;
		}
	    }
	}
	else {
	    if (errno == EACCES)
		ret = -1;
	}

done:
	if (ret == -1)
	    add_string(&strings, "!");
	else
	    add_string(&strings, (ret) ? "1" : "0");
    }

    *results = strings;
    return 0;
}
